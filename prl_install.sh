#!/bin/bash
prl_user=$(whoami)
sudo addgroup prl
sudo usermod -a -G prl $prl_user
sudo mkdir /sbin/prl
sudo cp prl.sh sprl.sh gprl.sh /sbin/prl/
sudo chown -R root:prl /sbin/prl
sudo chmod 750 /sbin/prl -R
sudo mkdir /var/log/.prl
sudo chown -R root:prl /var/log/.prl
sudo chmod 770 /var/log/.prl/
